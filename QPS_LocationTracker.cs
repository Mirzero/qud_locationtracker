using System;
using System.Text;
using XRL.World.Effects;

namespace XRL.World.Parts
{
    [Serializable]
    public class QPS_LocationTracker : IPart
    {
        public override void Register(GameObject Object)
        {
            Object.RegisterPartEvent(this, "EnteredCell");
            Object.RegisterPartEvent(this, "EndTurn");
            Object.RegisterPartEvent(this, "GetDisplayName");
            base.Register(Object);
        } 

        public GameObject Owner => ParentObject.Equipped ?? ParentObject.InInventory;

        public bool IsLost() 
        {
            if (Owner.GetEffect("Lost") is Lost lost) {
                return lost.Duration > 0;
            }
            return false;
        }

        public override bool FireEvent(Event E)
        {
            switch (E.ID)
            {
                case "GetDisplayName":
                    if (ParentObject.Understood())
                    {
                        UpdateCoordinates();
                        E.GetParameter<StringBuilder>("Postfix").Append(" &y[&R" + FormattedCoordinates + "&y]");
                    }
                    break;
            }

            return base.FireEvent(E);
        }


        public static readonly string UnknownCoordinates = "--, --, --";
        public string FormattedCoordinates = UnknownCoordinates;

        public static string CalculateParasangSection(int pX, int pY)
        {
            if(pX == 1 && pY == 1)
               return "C";

            var sX = pX == 0 ? "W" 
                   : pX == 1 ? ""
                   :           "E";

            var sY = pY == 0 ? "N" 
                   : pY == 1 ? ""
                   :           "S";

            return $"{sY}{sX}";
        }

        public void SetCoordinateString(string pS, string x, string y, string z)
        {
            var paraSec = string.IsNullOrWhiteSpace(pS) 
                        ? string.Empty
                        : pS + ", ";
            FormattedCoordinates = $"{paraSec} {x}, {y}, {z}";
        }

        public void ClearCoordinateString()
        {
            FormattedCoordinates = UnknownCoordinates;
        }

        public void UpdateCoordinates()
        {
            var curCell = ParentObject.CurrentCell ?? Owner?.CurrentCell;

            if (ParentObject.IsBroken()
                || curCell == null
                || FormattedCoordinates.Length <= 0
            )
            {
                ClearCoordinateString();
            }
            else
            {
                // World Map Tiles start in the top left at 0, 1
                var x = curCell.ParentZone.IsWorldMap() ? curCell.X : curCell.ParentZone.wX;   // World map tile coord - joppa is 11
                var y = curCell.ParentZone.IsWorldMap() ? curCell.Y : curCell.ParentZone.wY;   // World map tile coord - joppa is 22 
                var z = curCell.ParentZone.IsWorldMap() ? 9 : curCell.ParentZone.Z;            // 'ground' floor is 10

                // Parasang tiles are 0, 1, or 2 in each direction with NW being 0,0
                var pX = curCell.ParentZone.IsWorldMap() ? 1 : curCell.ParentZone.X;           // Parasang coord - 0-2 - Joppa is 1 (center)
                var pY = curCell.ParentZone.IsWorldMap() ? 1 : curCell.ParentZone.Y;           // Parasang coord - 0-2 - Joppa is 1 (center)

                var parasangSection = CalculateParasangSection(pX, pY);
                SetCoordinateString(parasangSection, x.ToString(), y.ToString(), z.ToString());
            }
        }
    }
}