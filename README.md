# Qud_LocationTracker
A mod for Caves of Qud that introduces a new artifact that displays the current map coordinates.
It's should be available from Argyve (might need a new game, or an inventory refresh), and can be created with tinkering, though it is uncertain where the tinkering recipe can be acquired.

## Steam Workshop
This mod is on the Steam Workshop for Caves of Qud!
https://steamcommunity.com/sharedfiles/filedetails/?id=2662655711

## Project status
This project has essentially fulfilled it's original goals. There is room for expansion upon the features (see roadmap), but time is scarce.
Time permitting, it will be updated as needed to continue working with updates to Caves of Qud.

## Roadmap
It would be nice to add higher tier versions of the artifact with expanded capabilities.
For example, the ability to "place beacon" or "mark location", which would add a note to a new section of the journal that is annotated with the coordinates of where it was created.
These extended features might need to be balanced by adding a cell slot, and using power.

## License
This mod is licensed under the GNU General Public License v3.0.
See license.md for more details.
